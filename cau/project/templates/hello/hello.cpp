#include "hello.hpp"

auto HelloWorld() -> std::string
{
    return std::string{"Hello, World!"};
}
