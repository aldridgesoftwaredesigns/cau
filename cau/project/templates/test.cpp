#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <{{ name }}.hpp>

// TODO: Delete as needed
using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::Ne;
using ::testing::Test;
using ::testing::TestWithParam;
using ::testing::Values;
