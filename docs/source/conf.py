"""
Document configuration file
"""
#ruff: noqa
import pathlib

import sphinx.ext.autodoc

logo = pathlib.Path("..")/".."/"pages"/"cau.svg"

project = "C++ Automation Utilities"
copyright = "2023, Aldridge Software Designs"
author = "Aldridge Software Designs"

extensions = ["sphinx_design", "sphinx.ext.autodoc", "sphinx.ext.coverage", "sphinx.ext.napoleon", "sphinx_click"]

templates_path = ["_templates"]
exclude_patterns = []

highlight_language = "c++"

html_theme = "sphinx_book_theme"
html_title = "FluidProps++"
html_logo = str(logo)
html_favicon = str(logo)
html_theme_options = {"home_page_in_toc": True}
html_static_path = ["_static"]

# Autodoc options
autoclass_content = "both"
autodoc_default_options = {
    "members": True,
    "member-order": "groupwise",
    "show-inheritance": True,
}

add_module_names = False

def no_object_description(obj):
    """
    Hack:
    Replace the `sphinx.ext.autodoc.object_description()` function
    in the `sphinx.ext.autodoc` module.
    """
    raise ValueError("No value for data/attributes")

sphinx.ext.autodoc.object_description = no_object_description
