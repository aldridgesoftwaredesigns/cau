CAUProject TOML File
====================

CAU allows specification of project properties via the `cauproject.toml` file. This file is used for defining a project's metadata, layout, initial build properties, and Gitlab CI configuration.
It is generally used to initialize a project, but will be expanded for further usage with integrate tools. The following gives an explanation of each block and values within the toml file. Default
values are shown. For all CLI argumens, the project file is passed implicitly or with an explicit path to a `cauproject.toml` file. If the file does not exist, to not break older projects, default
values are used. The `cauproject.toml` is assumed to be in the root of the project, and the CAU commands are being executed with the project root as current working directory.


.. code:: toml
    :number-lines:

    # Project metadata that gets populated all over the project
    [metadata]
    name = "" # Name of the project, used to create binaries
    version = "0.0.0" # Latest tagged version, at the moment, will need to manually bump with each new release
    licese = "MIT" # How do you want to license the project?
    repo_url = "" # URL to the code repository
    project_url = "" # URL to any other website (like docs) you want to link

    # Build data to setup `conan`
    [build]
    cross_build = false # Does this project cross-build to Windows? Set to false for Linux only
    clang_version = 14 # The major version number of clang to use, clang is default for Linux builds
    gcc_version = 11 # The major version of gcc to use, gcc is default for Windows cross-build
    cpp_standard = 20 # Which standard of C++ to use in the project.
    cmake_min_version = 3.29 # The cmake minimum version to use

    # Data to setup the `Gitlab CI`
    [gitlab]
    docker_image = "registry.gitlab.com/aldridgesoftwaredesigns/docker-images/cpp:latest" # Docker image to use throughout the pipeline
    code_quality = true # run code quality checks, false skips them
    sast = true # run sast checks, false skips them
    secrets = true # run secrets checks, false skips them
    self_managed = false # Runs on a self-mangaged Gitlab instance. Some specialized setup is required with self-mangaged instances

    # Project directory structure with respect to the project root
    [structure]
    source = "src" # Path to source files (translation units)
    headers = "include" # Path to header files
    tests = "tests" # Path to test files
    build = "build" # Build directory name
    maximum_module_depth = 2 # Maximum allowed depth of source modules (e.g. include/Vehicles/Planes is valid, include/Vehicles/Planes/SeaPlanes would not be)


